const { expect } = require('chai')
const request = require('supertest')

// App
const app = require('../app')
const agent = request.agent(app)

// Test suite
describe('API', () => {
  it('should say `Hello Hackathon`', async () => {
    const res = await request(app).get('/api')
    expect(res.status).to.equal(200)
    expect(res.text).to.be.a('string')
    expect(res.text).to.equal('Hello Hackathon')
  })

  it('should echo a query', done => {
    agent
      .get('/api')
      .query({ q: 'echo' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('echo')
        done()
      })
  })

  it('should find largest number', done => {
    agent
      .get('/api')
      .query({ q: '070e1f10: which of the following numbers is the largest: 68, 208' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('208')
        done()
      })
  })

  it('sum numbers', done => {
    agent
      .get('/api')
      .query({ q: '070e1f10: what is 5 minus 12' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('-7')
        done()
      })
  })

  context('should error', () => {
    it('on request to a non-existing endpoint', done => {
      agent
        .get('/api/bla')
        .expect(404)
        .end(done)
    })
  })
})
