module.exports = {
  hackathon: (req, res) => {
    let query = req.query
    let question = query.q
    let answer = ''

    if (/which of the following numbers is the largest/.test(question)) {
      let code = question.split(':')
      let numbers = code[2].split(',')
      for (let i = 0; i < numbers.length; i++) numbers[i] = parseInt(numbers[i], 10)
      answer = Math.max.apply(null, numbers)
    }

    if (/what is/.test(question)) {
      let method = question.split(':')

      let calculator = method[1].substr(9)

      if (/minus/.test(calculator)) {
        let numbers = calculator.split(' minus ')

        for (let i = 0; i < numbers.length; i++) {
          if (i == 0) {
            answer = parseInt(numbers[i], 10)
          } else {
            answer -= parseInt(numbers[i], 10)
          }
        }
      } else if (/plus/.test(calculator)) {
        let numbers = calculator.split(' plus ')

        for (let i = 0; i < numbers.length; i++) {
          if (i == 0) {
            answer = parseInt(numbers[i], 10)
          } else {
            answer += parseInt(numbers[i], 10)
          }
        }
      }
    }

    if (answer == '') {
      answer = question || 'Hello Hackathon'
    }

    // Log a query from game server
    if (process.env.NODE_ENV != 'test') console.log(query) // eslint-disable-line no-console

    // Answer the question
    res.status(200).send(String(answer))
  }
}
